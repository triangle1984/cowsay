import requests, os, sys
from datetime import datetime
class Cowsay():
    def _pogoda(self):
        apiurl = "http://api.openweathermap.org/data/2.5/find"
        appid = "22c7bf8e593c47b0cf88f390e8e5376a"
        params = {
                'q': sys.argv[1],
                'appid': appid,
                'units': 'metric',
                'lang': 'ru'
        }
        r = requests.get(apiurl, params=params, timeout=5)
        encode = r.json()
        self.temp = encode["list"][0]["main"]["temp"]
    def _time(self):
        now = datetime.now()
        self.time2 = datetime.strftime(datetime.now(), "%H:%M:%S")
    def cowsay(self):
        argv = ""
        self._pogoda()
        if int(self.temp) > 25:
            argv = "-d"
        self._time()
        os.system(f"cowsay {argv} погода: {self.temp}C, время: {self.time2} обновлений: $(pacman -Qu | wc -l)")
test = Cowsay()
test.cowsay()
